# Go言語でHelloWorld

## Golangのインストール

0. [Downloads - The Go Programming Language](https://golang.org/dl/)
0. ダウンロードしてインストールどうぞ
0. `> go version`


## IntelliJ IDEAで開発する

IntelliJインストールは割愛

0. Ctrl + Sで Settingsを開く
0. Plugins > Install JetBrains pluguin
0. Go languages をインストール、再起動


## プロジェクトを作る

0. File > New > Project > Go
0. 適当なディレクトリに適当なプロジェクト名で作成


## docker-composeつくっとく

0. docker-compose.ymlみてちょ


## docker立ち上げてアクセス

0. `> docker-compose up -d`
0. `> docker exec -it hello-golang bash`
0. `$ go version`


## run

`go run hello.go`


## build

0. `go build hello.go`
0. `./hello`
